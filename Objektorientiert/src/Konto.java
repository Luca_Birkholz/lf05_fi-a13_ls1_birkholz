
public class Konto {
	
	private String IBAN;
	private String kontoNummer;
	private double kontostand = 0;
	
	public void setIBAN(String IBAN) {
		this.IBAN = IBAN;
	}
	public String getIBAN() {
		return IBAN;
	}
	public void setKontoNummer(String kontoNummer) {
		this.kontoNummer = kontoNummer;
	}
	public String getKontoNummer() {
		return kontoNummer;
	}
	public double getKontostand() {
		return kontostand;
	}
	
	public void geldEinzahlen(double geld) {
		kontostand = kontostand + geld;
	}
	public void geldAuszahlen(double geld) {
		kontostand = kontostand - geld;
	}
	public void ueberweisung(double geld, Konto k) {
		this.kontostand = this.kontostand - geld;
		k.kontostand = k.kontostand + geld;
	}
}
