
public class ArrayHelper {

	public static int[] dreheArrayUm(int[] array) {
		
		int ram;
		int ram2; 
		
		for(int i = 0; i < 3; i++) {
			
			ram = array[i];
			ram2 = array[array.length - i - 1];
			
			array[i] = ram2;
			array[array.length - i - 1] = ram;
			
		}
		
		return array;
		
	}

	public static String convertArrayToString(int[] zahlen) {
		
		String ausgabe = "";
		
		for(int i = 0; i < 4; i++) {
			
			ausgabe += zahlen[i] + ", ";
			if(i == 3) ausgabe += zahlen[4];
			
		}
		
		return ausgabe;
		
	}
	
	public static void main(String[] args) {
		
		int[] zahl = {1, 2, 3, 4, 5};
		
		zahl = dreheArrayUm(zahl);
		
		String ausgabe = convertArrayToString(zahl);
		
		System.out.println(ausgabe);
		
	}

}
