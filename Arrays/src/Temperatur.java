import java.util.Scanner;

public class Temperatur {

	public static double[][] calc(int laenge, double[][] temps) {
		
		//Bef�llung zweite Spalte
		for(int i = 0; i < laenge; i++) {
			
			temps[i][1] = (5.0 / 9.0) * (temps[i][0] - 32);
		}
		
		return temps;
		
	}
	
	
	
	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		int laenge = tastatur.nextInt() + 2;
		
		double[][] temps = new double [laenge][2];
		
		
		//Bef�llung
		for(int i = 0; i < laenge; i++) {
			
			temps[i][0] = 10.0 * i;
			
		}
		
		
		//Methode f�r zweite Spalte
		temps = calc(laenge, temps);
		
		
		//Ausgabe
		String ausgabe = "";
		
		for(int i = 0; i < laenge - 1; i++) {
			
			ausgabe += temps[i][0] + " | " + temps[i][1] + "\n";
			
		}
		System.out.println("F    | C");
		System.out.println(ausgabe);
		
	}

}
