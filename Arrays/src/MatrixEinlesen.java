import java.util.Scanner;

public class MatrixEinlesen {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		int zeilen = tastatur.nextInt();
		int spalten = tastatur.nextInt();
		
		double[][] matrix = new double[zeilen][spalten];
		
		System.out.println("Angefangen wird mit der ersten Spalte.");
		
		for(int i = 0; i < zeilen; i++) {
			for(int j = 0; j < spalten; j++) {
				
				System.out.println("Bitte n�chsten Wert eingeben: ");
				matrix[i][j] = tastatur.nextDouble();
				
			}
			System.out.println("N�chste Spalte!");
		}
		
		String ausgabe = "";
		
		for(int i = 0; i < zeilen; i++) {
			for(int j = 0; j < spalten; j++) {
				
			ausgabe += matrix[i][j] + " | " + matrix[i][j] + "\n";
				
			}
		}
		System.out.println(ausgabe);
	}

}
