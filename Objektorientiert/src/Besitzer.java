
public class Besitzer {

		private String vorname = "";
		private String name = "";
		private Konto k1;
		private Konto k2;
		
		public void setVorname(String vorname) {
			this.vorname = vorname;
		}
		public String getVorname() {
			return vorname;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getName() {
			return name;
		}
		public void setKonto1(Konto konto1) {
			this.k1 = konto1;
		}
		public Konto getKonto1() {
			return k1;
		}
		public void setKonto2(Konto konto2) {
			this.k2 = konto2;
		}
		public Konto getKonto2() {
			return k2;
		}
		
		public void gesamtUebersicht() {
			System.out.println( "Konto 1: " + getKonto1().getIBAN() + "\nKonto 2: " + getKonto2().getIBAN() );
		}
		public void gesamtGeld() {
			double gesamt = getKonto1().getKontostand() + getKonto2().getKontostand();
			System.out.print( gesamt );
		}
}
