import java.util.Scanner;

public class Taschenrechner {

	public static void main(String[] args) {
		Scanner keylogger = new Scanner(System.in);
		System.out.println("Geben Sie die erste Zahl ein: ");
		double a = keylogger.nextDouble();
		System.out.println("Geben Sie die zweite Zahl ein: ");
		double b = keylogger.nextDouble();
		System.out.println("Geben Sie die Rechenart ein: (+/-/*/:)");
		char r = keylogger.next().charAt(0);
		double erg = 0.0;
		
		switch (r) {
		case '+':
			erg = a + b;
			System.out.println("Das Ergebnis lautet: " + erg);
			break;
		case '-':
			erg = a - b;
			System.out.println("Das Ergebnis lautet: " + erg);
			break;
		case '*':
			erg = a * b;
			System.out.println("Das Ergebnis lautet: " + erg);
			break;
		case ':':
			erg = a / b;
			System.out.println("Das Ergebnis lautet: " + erg);
			break;
		default: 
			System.out.println("Fehlerhafte Eingabe!");
			break;
		}

	}

}
