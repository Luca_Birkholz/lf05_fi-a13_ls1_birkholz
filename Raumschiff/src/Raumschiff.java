import java.util.ArrayList;

public class Raumschiff {
	
	//Attribute
	private int photonentorpedoAnzahl = 0;
	private int energieversorgungInProzent = 0;
	private int schildeInProzent = 0;
	private int huelleInProzent = 0;
	private int lebenserhaltungssystemeInProzent = 0;
	private int androidenAnzahl = 0;
	private String schiffsname = "";
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	//Konstruktoren
	public Raumschiff() {
	}
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}
	
	//Verwaltungsmethoden
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}
	public int getSchildeInProzent() {
		return schildeInProzent;
	}
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}
	public int getHuelleInProzent() {
		return huelleInProzent;
	}
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}
	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	public String getSchiffsname() {
		return schiffsname;
	}
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	
	//Methoden
	/***
	 * Vorraussetzung: Uebergebenes Objekt der Klasse Ladung existiert
	 * Effekt: F�gt neues Ladungsobjekt dem Ladungsverzeichnis zu
	 * @param neueLadung Objekt der Klasse Ladung
	 */
	public void addLadung(Ladung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);
	}
	/***
	 * Vorraussetzung: Uebergebenes Objekt der Klasse Raumschiff existiert
	 * Effekt: Reduziert Torpedoanzahl um 1 und ruft Treffermethode f�r Zielraumschiff auf
	 * @param r Objekt der Klasse Raumschiff
	 */
	public void photonentorpedoSchiessen(Raumschiff r) {
		if(this.photonentorpedoAnzahl > 0) {
			this.photonentorpedoAnzahl -= 1;
			System.out.println("\nPhotonentorpedo auf " + r.getSchiffsname() + " abgeschossen");
			r.treffer(r);
		}
		else {
			System.out.println("\nNicht genug Photonentorpedos!");
		}
	}
	/***
	 * Vorraussetzung: Uebergebenes Objekt der Klasse Raumschiff existiert
	 * Effekt: Reduziert Energie um 50% und ruft Treffermethode f�r Zielraumschiff auf
	 * @param r Objekt der Klasse Raumschiff
	 */
	public void phaserkanoneSchiessen(Raumschiff r) {
		if (this.energieversorgungInProzent < 50) {
			System.out.println("\nNicht genug Energie!");
		}
		else {
			this.energieversorgungInProzent -= 50;
			System.out.println("\nPhaserkanone auf " + r.getSchiffsname() + " abgeschossen.");
			r.treffer(r);
		}
	}
	/***
	 * Vorraussetzung: Uebergebenes Objekt der Klasse Raumschiff existiert
	 * Effekt: Reduziert Schilde des getroffenen Raumschiffs um 50, sonst die Huelle um 50
	 * @param r Objekt der Klasse Raumschiff
	 */
	private void treffer(Raumschiff r) {
		r.schildeInProzent -= 50;
		System.out.println("\n" + r.getSchiffsname() + " wurde getroffen!");
		if(r.schildeInProzent <= 0) {
			r.huelleInProzent -= 50;
			r.energieversorgungInProzent -= 50;
			if(r.huelleInProzent <= 0) {
				r.lebenserhaltungssystemeInProzent = 0;
				r.nachrichtAnAlle("\nNotruf: Lebenserhaltung vernichtet!");
			}
		}
	}
	/***
	 * Effekt: Sendet eine Nachricht an alle (in die Konsole)
	 * @param message Die zu sendende Nachricht
	 */
	public void nachrichtAnAlle(String message) {
		this.broadcastKommunikator.add(message);
		System.out.println("\n" + this.schiffsname + ": " + message);
	}
	public ArrayList<String> eintraegeLogbuchZurueckgeben() {
		return(this.broadcastKommunikator);
	}
	/***
	 * Effekt: Gibt aktuelle Attribute des Raumschiffs in der Konsole aus
	 */
	public void zustandRaumschiff() {
		System.out.println("\n" + this.schiffsname + ":");
		System.out.println("Torpedos: " + this.photonentorpedoAnzahl);
		System.out.println("Energie: " + this.energieversorgungInProzent + "%");
		System.out.println("Schilde: " + this.schildeInProzent + "%");
		System.out.println("Huelle: " + this.huelleInProzent + "%");
		System.out.println("Lebenserhaltung: " + this.lebenserhaltungssystemeInProzent + "%");
		System.out.println("Androiden: " + this.androidenAnzahl);
	}
	/***
	 * Effekt: Gibt aktuelles Ladungsverzeichnis aus
	 */
	public void ladungsverzeichnisAusgeben() {
		for(int i = 0; i < this.ladungsverzeichnis.size(); i++) {
			System.out.printf("%s: %d Stueck \n", this.ladungsverzeichnis.get(i).getBezeichnung(), this.ladungsverzeichnis.get(i).getMenge());
		}
	}
}