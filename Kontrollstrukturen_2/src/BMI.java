import java.util.*;


public class BMI {

	public static void main(String[] args) {
		Scanner keylogger = new Scanner(System.in);
		System.out.println("Welches Geschlecht haben Sie? (m/w)");
		char g = keylogger.next().charAt(0);
		System.out.println("Wie groß sind Sie? (in m)");
		double m = keylogger.nextDouble();		
		System.out.println("Wie schwer sind Sie? (in kg)");
		double k = keylogger.nextDouble();
		double bmi = k/(m*m);
		
		if(bmi < 20 && g == 'm') {
			System.out.println("Sie sind untergewichtig! Ihr BMI: " + bmi);
		}
		if(bmi >= 20 && bmi <= 25 && g == 'm') {
			System.out.println("Sie haben ein normales Gewicht! Ihr BMI: " + bmi);
		}
		if(bmi > 25 && g == 'm') {
			System.out.println("Sie sind übergewichtig! Ihr BMI: " + bmi);
		}
		if(bmi < 19 && g == 'w') {
			System.out.println("Sie sind untergewichtig! Ihr BMI: " + bmi);
		}
		if(bmi >= 19 && bmi <= 24 && g == 'w') {
			System.out.println("Sie haben ein normales Gewicht! Ihr BMI: " + bmi);
		}
		if(bmi > 24 && g == 'w') {
			System.out.println("Sie sind übergewichtig! Ihr BMI: " + bmi);
		}
		else { System.out.println("Fehlerhafte Eingabe!"); }
	}

}
