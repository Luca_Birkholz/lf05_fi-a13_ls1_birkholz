
public class KontoTest {

	public static void main(String[] args) {
		
		Konto konto1 = new Konto();
		Konto konto2 = new Konto();
		
		konto1.setIBAN("1000");
		konto1.setKontoNummer("1");
		konto2.setIBAN("2000");
		konto2.setKontoNummer("2");
		System.out.println("IBAN: " + konto1.getIBAN() + "\nKontonummer: " + konto1.getKontoNummer() + "\n" + konto1.getKontostand() + "�");
		konto1.geldEinzahlen(200);
		System.out.println(konto1.getKontostand() + "�");
		konto1.geldAuszahlen(100);
		System.out.println(konto1.getKontostand() + "�");
		konto1.ueberweisung(50, konto2);
		System.out.println("Konto 1: " + konto1.getKontostand() + "�");
		System.out.println("Konto 2: " + konto2.getKontostand() + "�");
		
		Besitzer b1 = new Besitzer();
		
		b1.setVorname("Mark");
		b1.setName("Dark");
		b1.setKonto1(konto1);
		b1.setKonto2(konto2);
		System.out.println("\n\nVorname: " + b1.getVorname() + "\nName: " + b1.getName());
		b1.gesamtUebersicht();
		System.out.print("Gesamtgeld: ");
		b1.gesamtGeld();
		System.out.print("�");
	}

}
