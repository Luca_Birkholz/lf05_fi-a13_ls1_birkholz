
public class lotto {

	public static void main(String[] args) {
		
		int[] lotto = {3, 7, 12, 18, 37, 42};

		System.out.print("[ ");
		
		for(int i = 0; i < 6; i++) {
			
			System.out.print(lotto[i] + " ");
			
		}
		
		System.out.println("]");
		
		boolean test = false;
		
		for(int i = 0; i < 6; i++) {
			
			if(lotto[i] == 12) System.out.println("Die Zahl 12 ist in der Ziehung enthalten.");
			if(lotto[i] == 13) test = true;
		}
		
		if(test == false) System.out.println("Die Zahl 13 ist nicht in der Ziehung enthalten.");
		
	}

}
