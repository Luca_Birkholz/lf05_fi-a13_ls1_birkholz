
public class Ladung {

	//Attribute
	private String bezeichnung = "";
	private int menge = 0;
	
	//Konstruktor
	public Ladung() {
	}
	public Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}
	
	//Verwaltungsmethoden
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	public String getBezeichnung() {
		return bezeichnung;
	}
	public void setMenge(int menge) {
		this.menge = menge;
	}
	public int getMenge() {
		return menge;
	}
	/***
	 * Effekt: Wandelt das Ladungsobjekt in einen String um
	 */
	@Override
	public String toString() {
		String returnString = "Ladung: " + this.bezeichnung + "; " + this.menge;
		return returnString;
	}
}