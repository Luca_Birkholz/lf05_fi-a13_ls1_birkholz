﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args) {
    	char endlos = 'Y';
    	
    	while (endlos == 'Y' || endlos == 'y') {
    		
    		double zuZahlenderBetrag = fahrkartenbestellungErfassen();

    		double rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
    	
    		fahrkartenAusgeben(rueckgabebetrag);
    		
    		Scanner tastatur = new Scanner(System.in);
    		System.out.println("Möchten Sie weitere Fahrkarten kaufen? (Y/N): ");
    		endlos = tastatur.next().charAt(0);
    	}
    }
    
    public static double fahrkartenbestellungErfassen() {
 	   Scanner tastatur = new Scanner(System.in);
 	   double preise[] = {2.9, 3.3, 3.6, 1.9, 8.6, 9.0, 9.6, 23.5, 24.3, 24.9};
 	   String tickets[] = {"Einzelfahrschein Regeltarif AB (1)", 
 	 	   		"Einzelfahrschein Regeltarif BC (2)",
 	 	   		"Einzelfahrschein Regeltarif ABC (3)",
 	 	   		"Kurzstrecke (4)",
 	 	   		"Tageskarte Regeltarif AB (5)", 
 	 	   		"Tageskarte Regeltarif BC (6)",
 	 	   		"Tageskarte Regeltarif ABC (7)",
 	 	   		"Kleingruppen-Tageskarte Regeltarif AB (8)",
 	 	   		"Kleingruppen-Tageskarte Regeltarif BC (9)",
 	 			"Kleingruppen-Tageskarte Regeltarif ABC (10)"};
 	   System.out.print("Wählen Sie Ihre Wunschfahrkarte für Berlin aus:\n");
 	   for(int i = 0; i < tickets.length; i++) {
 		   System.out.println(tickets[i] + " [" + preise[i] + "0 EUR]");
 	   }
 	   System.out.println("Ihre Wahl: ");
 	   int auswahl = tastatur.nextInt();
 	   double einzelpreis = 0.0;
 	   
 	   
 	   
 	   System.out.print("Anzahl der Tickets: ");
 	   byte anzahl = tastatur.nextByte();
 	   
 	   double zuZahlenderBetrag = preise[auswahl - 1] * anzahl;
 	   return zuZahlenderBetrag;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	double eingezahlterGesamtbetrag = 0.0;
    	Scanner tastatur = new Scanner(System.in);
    	
    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
    		System.out.printf("Noch zu zahlen: " + "%.2f" + " EURO\n" , (zuZahlenderBetrag - eingezahlterGesamtbetrag));
    		System.out.print("Eingabe (mind. 5 CENT, höchstens 2 EURO): ");
    		double eingeworfeneMünze = tastatur.nextDouble();
    		eingezahlterGesamtbetrag += eingeworfeneMünze;
    	}
    	
    	double rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
    	return rueckgabebetrag;
    }
    
    public static void fahrkartenAusgeben(double rueckgabebetrag) {
    System.out.println("\nFahrschein wird ausgegeben");
    for (int i = 0; i < 8; i++) {
       System.out.print("=");
       try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    }
    System.out.println("\n\n");
    
    rueckgeldAusgeben(rueckgabebetrag);
    
    System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                       "vor Fahrtantritt entwerten zu lassen!\n"+
                       "Wir wünschen Ihnen eine gute Fahrt.");
    }
    
    public static void rueckgeldAusgeben(double rueckgabebetrag) {
    	   
           if(rueckgabebetrag > 0.0)
           {
        	   System.out.printf("Der Rückgabebetrag in Höhe von " + "%.2f" + " EURO\n" , rueckgabebetrag);
        	   System.out.println("wird in folgenden Münzen ausgezahlt:");

               while(rueckgabebetrag >= 2.0) // 2 EURO-Münzen
               {
            	  System.out.println("2 EURO");
    	          rueckgabebetrag -= 2.0;
               }
               while(rueckgabebetrag >= 1.0) // 1 EURO-Münzen
               {
            	  System.out.println("1 EURO");
    	          rueckgabebetrag -= 1.0;
               }
               while(rueckgabebetrag >= 0.5) // 50 CENT-Münzen
               {
            	  System.out.println("50 CENT");
    	          rueckgabebetrag -= 0.5;
               }
               while(rueckgabebetrag >= 0.2) // 20 CENT-Münzen
               {
            	  System.out.println("20 CENT");
     	          rueckgabebetrag -= 0.2;
               }
               while(rueckgabebetrag >= 0.1) // 10 CENT-Münzen
               {
            	  System.out.println("10 CENT");
    	          rueckgabebetrag -= 0.1;
               }
               while(rueckgabebetrag >= 0.05)// 5 CENT-Münzen
               {
            	  System.out.println("5 CENT");
     	          rueckgabebetrag -= 0.05;
               }
           }
    }
}